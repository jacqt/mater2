(function(){
    angular.module('mater2.controllers').controller('currentPlayersController', ['$scope', 'dataStreamService','communicatorService', controller]);


    function controller($scope, dataStreamService, communicatorService){
        $scope.bughouseInfo = {
            title: 'Bughouse', 
            state : {
                curBugGames: [],
                curPartners: [],
                allPlayers: []
            }
        };
        $scope.chessInfo = {'title': 'Chess'};
        $scope.tabs = [
            $scope.bughouseInfo,
            $scope.chessInfo
        ];
        var nameObjMap= {
            'bughouse' : $scope.bughouseInfo
        }

        function onOpen(event){
            console.log('Opened socket for our data stream')
        }
        function onMessage(event){
            var updateObj = JSON.parse(event.data);
            nameObjMap[updateObj.name].state = expand(updateObj);

        }

        function expand(obj){
            if (Array.isArray(obj)){
                if (obj.length == 0){
                    return ['-']
                }
                return obj;
            }
            else if (typeof(obj)== 'string'){
                return obj
            } else {
                console.log(typeof(obj));
                for (var k in obj){
                    obj[k] = expand(obj[k]);
                }
                return obj;
            }
        }

        $scope.openChat =function(bugname){
            var name = /[^A-Za-z]*([A-Za-z]*)[^A-Za-z]*/.exec(bugname)[1];
            communicatorService.fireEvent('openChatGroup', {
                name: name
            });
        }

        dataStreamService.addHandler('message', onMessage);
        dataStreamService.addHandler('open', onOpen);
        console.log(dataStreamService);
    }
})()

