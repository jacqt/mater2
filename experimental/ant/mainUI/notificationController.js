/**Controller for a notifications dropdown
 */

(function(){
    console.log('Initializing notificationController...');
    angular.module('mater2.controllers')
        .controller('notificationController',
            ['$scope', 'icsSocket', controller]);

    function controller($scope, icsSocket){

        var challengedRe = /Challenge: ([A-Za-z]+) (\(.*?\))* ([A-Za-z]+) (\(.*?\))* ([A-Za-z]*) ([A-Za-z]*) ([0-9]*) ([0-9]*).*/;
        var partnerReqRe = /[A-Za-z]+ offers to be your bughouse partner.*/;
        var updateRe = [challengedRe, partnerReqRe];

        var pendingReadChallengeRe = / ([0-9]*):.* challenge: ([A-Za-z]+) (\(.*?\))* ([A-Za-z]+) (\(.*?\))* ([A-Za-z]*) ([A-Za-z]*) ([0-9]*) ([0-9]*).*/;
        var pendingReadPartnerReqRe = / ([0-9]*): ([A-Za-z]+) is offering to be bughouse partners.*/; 
        $scope.numberOfNewNotifications = 0;

        $scope.notifications = [];

        $scope.onToggle = function(open){
            if (open){
                $scope.numberOfNewNotifications = 0;
            }
        }

        $scope.onMatchReceived = function(data){
            var opponent = data.name;
            var time = data.time;
            var increment = data.increment;
        }

        $scope.accept = function(notif){
            console.log(notif);
            icsSocket.sendLine('accept ' + notif.offerNumber);
            icsSocket.sendLine('pending');
            //$scope.notifications.splice($scope.notifications.indexOf(notif),1);
        }

        function onMessage(data){
            var currentMode = null;
            var lines = data.data.split('\n');
            for (var i = 0; i != lines.length; ++i){
                var line = lines[i];
                if (currentMode == 'readingOffers'){
                    var m = pendingReadChallengeRe.exec(line);
                    if (m != null){
                        var offerNumber = m[1];
                        var challengeIssuer = m[2];
                        var challengeIssuerRating = m[3];
                        var challengeRecipient = m[4];
                        var challengeRecipientRating = m[5];
                        var ratedStr = m[6]
                        var variant = m[7];
                        var time = m[8];
                        var inc = m[9];
                        var notifObj = {
                            title: challengeIssuer + ' ' + challengeIssuerRating,
                            details: ratedStr + ' ' + variant + ' ' + time + ' ' + inc,
                            offerNumber: offerNumber,
                            canAccept: true
                        };
                        $scope.notifications.push(notifObj);
                    }

                    m = pendingReadPartnerReqRe.exec(line);
                    if (m != null){
                        var offerNumber = m[1];
                        var offerMaker = m[2];
                        var notifObj = {
                            title: offerMaker + ' offers to be bughouse partners',
                            offerNumber: offerNumber,
                            canAccept: true
                        };
                        $scope.notifications.push(notifObj);
                    }
                } else {
                    for (var j = 0; j != updateRe.length; ++j){
                        var re = updateRe[j];
                        var m = re.exec(line);
                        if (m != null){
                            icsSocket.sendLine('pending');
                            $scope.numberOfNewNotifications += 1;
                            break;
                        }
                    }

                    m = /Offers FROM other players:.*/.exec(line);
                    if (m != null){
                        currentMode = 'readingOffers';
                        $scope.notifications = [];
                    }
                    m = /There are no offers pending FROM other players.*/.exec(line);
                    if (m != null){
                        $scope.notifications = [{
                            canAccept: false,
                            title: 'No current offers'
                        }];
                    }
                }
            }
        }

        icsSocket.addHandler('message', onMessage);
    }

})();
