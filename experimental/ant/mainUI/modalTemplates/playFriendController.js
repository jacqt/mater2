angular.module('mater2.controllers').controller('playFriendController', function ($scope, $modalInstance, items) {
    $scope.variants = [
        { name : 'Chess', value: ' '},
        { name : 'Bughouse', value : 'bughouse' },
        { name : 'Crazyhouse', value : 'zh' }
    ];
    $scope.choice = {
        name : $scope.variants[0].name,
        variant: $scope.variants[0].value,
        time: '2',
        increment : '12'
    };

    $scope.ok = function () {
        console.log($scope.choice);
        $modalInstance.close($scope.choice);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});
