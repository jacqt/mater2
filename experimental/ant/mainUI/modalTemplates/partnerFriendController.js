angular.module('mater2').controller('partnerFriendController', function ($scope, $modalInstance, items) {
    $scope.choice = {
        name : '',
    };

    $scope.ok = function () {
        console.log($scope.choice);
        $modalInstance.close($scope.choice);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});
