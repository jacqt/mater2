angular.module('mater2').controller('createGameController', function ($scope, $modalInstance, items) {
    $scope.variants = [
        { name : 'Chess', value: ' '},
        { name : 'Bughouse', value : 'bughouse' },
        { name : 'Crazyhouse', value : 'zh' }
    ];
    $scope.choice = {
        variant: $scope.variants[1].value,
        time: '3',
        increment : '0'
    };

    $scope.ok = function () {
        console.log($scope.choice);
        $modalInstance.close($scope.choice);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});
