/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 * The gateway that the user uses to start a game
 */

(function(){
    angular.module('mater2.controllers')
    .controller('playGameController',
        ['$scope', 'icsSocket', '$modal', playGameController])
    .animation('.slide', animator);

    function animator(){
        var NG_HIDE_CLASS = 'ng-hide';
        return {
            beforeAddClass: function(element, className, done) {
                if(className === NG_HIDE_CLASS) {
                    element.slideUp(done);
                }
            },
            removeClass: function(element, className, done) {
                if(className === NG_HIDE_CLASS) {
                    console.log(element);
                    element.hide().slideDown(done);
                }
            }
        }
    }

    function playGameController($scope, icsSocket, $modal){
        $scope.openPlayFriend = function(){
            var modalInstance = $modal.open({
                templateUrl: 'app/mainUI/modalTemplates/playFriendTemplate.html',
                controller: 'playFriendController',
                resolve: {
                    items: function () {
                        return []
                    }
                }
            });
            modalInstance.result.then(
                function success(matchDetails) {
                    $scope.sendMatch(matchDetails);
                }, 
                function cancel() {
                });
        }

        $scope.openCreateGame = function(){
            var modalInstance = $modal.open({
                templateUrl: 'app/mainUI/modalTemplates/createGameTemplate.html',
                controller: 'createGameController',
                resolve: {
                    items: function () {
                        return []
                    }
                }
            });
            modalInstance.result.then(
                function success(seekDetails) {
                    $scope.sendSeek(seekDetails);
                }, 
                function cancel() {
                });
        }

        $scope.openPartnerFriend = function(){
            var modalInstance = $modal.open({
                templateUrl: 'app/mainUI/modalTemplates/partnerFriendTemplate.html',
                controller: 'partnerFriendController',
                resolve: {
                    items: function () {
                        return []
                    }
                }
            });
            modalInstance.result.then(
                function success(partner) {
                    icsSocket.sendLine('partner ' + partner.name);
                }, 
                function cancel() {
                });

        }
        $scope.sendMatch = function(matchDetails){
            var command = 'match ' + matchDetails.name +
                ' ' + matchDetails.time + ' ' + matchDetails.increment 
                + ' ' + matchDetails.variant;
            icsSocket.sendLine(command);
        }
        $scope.sendSeek = function(matchDetails){
            var command = 'seek ' + ' ' + matchDetails.time 
                + ' ' + matchDetails.increment 
                + ' ' + matchDetails.variant;
            icsSocket.sendLine(command);
        }
    }
})();
