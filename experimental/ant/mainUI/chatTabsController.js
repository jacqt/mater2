(function(){
    angular.module('mater2.controllers').controller('chatTabsController', ['$scope', 'icsSocket', 'communicatorService', controller]);
    var RETURN_KEY = 13;

    function controller($scope, icsSocket, communicatorService){
        function findOrCreateChatGroup(name){
            for (var i = 0; i != $scope.chatGroups.length; ++i){
                var chatGroup = $scope.chatGroups[i];
                if (chatGroup.talkingTo == name){
                    return chatGroup;
                }
            }
            var chatGroup = {
                heading: name,
                talkingTo: name,
                currentText: '',
                active: true,
                messages : []
            }
            $scope.chatGroups.push(chatGroup);
            return chatGroup;

        }

        $scope.chatGroups = [ ];

        $scope.onKeyPress = function(ev, chatGroup){
            if (ev.keyCode == RETURN_KEY){
                $scope.sendMsg(chatGroup);
            }
        }

        communicatorService.addHandler('openChatGroup', function(data){
            var chatGroup = findOrCreateChatGroup(data.name);
        });

        icsSocket.addHandler('message', function(data){
            var data = data.data;
            var lines = data.split('\n');
            var getToldRe = /^([A-Za-z]+)(\(.*?\))* tells you: (.*)/;
            var toldSomeoneRe = /\(told (.*)\)/;
            var sessionStartRe = /\*\*\* Starting BICS session as ([A-Za-z]*) \*\*\*/
            for (var i = 0; i != lines.length; ++i){
                var line = lines[i];
                var m = getToldRe.exec(line.trim());
                if (m != null){
                    var sender = m[1];
                    var msg = m[3];
                    var chatGroup = findOrCreateChatGroup(sender);
                    chatGroup.messages.push({
                        name: sender,
                        msg: msg
                    });
                }
                m = toldSomeoneRe.exec(line)
                if (m != null){
                    var name = m[1];
                    var chatGroup = findOrCreateChatGroup(name);
                    chatGroup.messages.push({
                        name: $scope.myName,
                        msg: chatGroup.sentText
                    })
                    chatGroup.sentText = '';
                }

                m = sessionStartRe.exec(line);
                if (m != null){
                    $scope.myName = m[1];
                }
            }
        });


        communicatorService.addHandler('sentTell', function(data){
            var sentTo = data.name;
            var msg = data.msg;
            var chatGroup = findOrCreateChatGroup(sentTo);
            chatGroup.messages.push({
            });
        });

        $scope.sendMsg = function(chatGroup){
            var cmd = 'tell ' + chatGroup.talkingTo + ' ' + chatGroup.currentText;

            chatGroup.sentText = chatGroup.currentText;
            chatGroup.currentText = '';
            icsSocket.sendLine(cmd);
        }
    }
})();

