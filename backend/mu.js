// vim: set et ts=2 sts=2 sw=2:

var seed = "mi";
var goal = "mu";

function apply(rule_fn, str, next_arr) {
  var results = rule_fn(str);
  if (results !== undefined) {
    for (var i = 0; i < results.length; ++i) {
      next_arr.push(results[i]);
    }
  }
}

// Returns array of all possible replacements.
function special_replace(base_str, substr, new_str) {
  var possibilities = [];
  for (var i = 0; i < base_str.length; ++i) {
    var first_half = base_str.substring(0, i);
    var second_half = base_str.substring(i);
    if (second_half.indexOf(substr) === 0) {
      var third_chunk = second_half.substring(substr.length);
      possibilities.push(first_half + new_str + third_chunk);
    }
  }
  return possibilities;
}

function rule1(str) {
  if (str.charAt(str.length-1) === "i") {
    return [str + "u"];
  }
  return [];
}

function rule2(str) {
  if (str.charAt(0) === "m") {
    return [str + str.substring(1)];
  }
  return [];
}

function rule3(str) {
  return special_replace(str, "iii", "u");
}

function rule4(str) {
  return special_replace(str, "uu", "");
}

function bfs(str) {
  var rules = [rule1, rule2, rule3, rule4];
  var next_arr = [];
  if (str.length <= 0) {
    console.error("str.length == 0");
    return;
  }
  for (var i = 0; i < rules.length; ++i) {
    apply(rules[i], str, next_arr);
  }
  console.log(next_arr);
  return next_arr;
}

function iterate(iter) {
  if (iter.next.length <= 0) {
    return;
  }
  var next = iter.next.shift();
  if (iter.memo.indexOf(next) >= 0) {
    return;
  }
  iter.memo.push(next);
  iter.next = iter.next.concat(bfs(next));
console.warn(iter);
}

var next = [seed];
var memo = [];
var iter = {"next": [seed], "memo": []};
while (iter.next.length != 0) {
  console.log("iterating...");
  iterate(iter);
  if (iter.memo.indexOf(goal) >= 0) {
    break;
  }
}
console.log("Done!");
console.log(iter.memo);
