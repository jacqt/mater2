/*
 * Copyright 2015 (c) BughouseClub.com
 *
 * Author: Alex Guo (chessnut@outlook.com).
 *
 * Purpose: backend server that sets up arena for players. Does not actually
 *          do auto-partner or auto-match, but suggests to the users which
 *          people or teams they can play.
 */

var http = require('http');
var net = require('net');
var sockjs = require('sockjs');

// TODO: put magic constants in config file.
var ArenaBotPort = 3005;
var ArenaBotTestPort = 5005;

var ports = {
  production: ArenaBotPort,
  test: ArenaBotTestPort,
};

var arena = {
  // Each obj in 'single_pool' has: {
  //   'username': <string>,
  //   'connection': <connection Obj>,
  //   'fingerprint': <string>,
  // };
  single_pool: [],

  // Each obj in 'team_pool' has: {
  //   'usernames': [<string>, ...],
  //   'team_leader_conn': <connection Obj>,
  //   'fingerprint': <string>,
  // };
  team_pool: [],
};

var arena_event_handlers = {
  //---------------------------------------------------------------------------
  // Actual commands.
  'seek partner': onNewSingle,
  'seek bug': onNewTeam,

  'unseek partner': onUnseekSingle,
  'unseek bug': onUnseekTeam,

  //---------------------------------------------------------------------------
  // Debug the single pool.
  'who s': onWhoSingle,
  'who singles': onWhoSingle,

  // Debug the team pool.
  'who b': onWhoTeam,
  'who bug': onWhoTeam,
};

// Returns a random number in the range [0, 'limit').
function getRandomIdx(limit) {
  var arbitrarily_large_num = 1000000;
  var random_num = (Math.random() * arbitrarily_large_num) % limit;
  return Math.floor(random_num);
}

function fingerprintTeam(msg) {
  var fingerprint = msg.usernames.sort().join();
  return fingerprint;
}

function addTeam(msg, connection) {
  var fingerprint = fingerprintTeam(msg);
  for (var i = 0; i < arena.team_pool.length; ++i) {
    if (fingerprint == arena.team_pool[i].fingerprint) {
      return arena.team_pool[i];
    }
  }
  var new_team = {
    usernames: msg.usernames,
    fingerprint: fingerprint,
    team_leader_conn: connection,
  };
  arena.team_pool.push(new_team);
  return new_team;
}

function onNewTeam(msg, connection) {
  if (msg.usernames === undefined) {
    return { type: 'error', msg: 'Need "usernames" field.' };
  }
  var new_team = addTeam(msg, connection);

  var num_teams = arena.team_pool.length;
  if (num_teams <= 1) {
    return {type: 'wait', msg: 'Please wait'};
  }

  var suggestion;
  do {
    var random_idx = getRandomIdx(num_teams);
    suggestion = arena.team_pool[random_idx];
  } while (new_team.fingerprint === suggestion.fingerprint);

  suggestion.team_leader_conn.write(JSON.stringify({
    type: 'team_suggestion',
    team_suggestion: new_team.usernames,
  }));
  return {type: 'team_suggestion',
          team_suggestion: suggestion.usernames};
}

function fingerprintSingle(msg) {
  var fingerprint = msg.username;
  return fingerprint;
}

function addSingle(msg, connection) {
  var fingerprint = fingerprintSingle(msg);
  for (var i = 0; i < arena.single_pool.length; ++i) {
    if (fingerprint === arena.single_pool[i].fingerprint) {
      return arena.single_pool[i];
    }
  }
  var new_user = { connection: connection,
                     username: msg.username,
                  fingerprint: fingerprint};
  arena.single_pool.push(new_user);
  return new_user;
}

function removeSingle(connection) {
  var result = false;
  for (var i = 0; i < arena.single_pool.length; ++i) {
    if (connection === arena.single_pool[i].connection) {
      arena.single_pool.splice(i, 1);
      --i; // Now that the array has shortened, adjust the index.
      result = true; // We actually removed someone.
    }
  }
  return result;
}

function removeTeam(connection) {
  var result = false;
  for (var i = 0; i < arena.team_pool.length; ++i) {
    if (connection === arena.team_pool[i].team_leader_conn) {
      arena.team_pool.splice(i, 1);
      --i; // Now that the array has shortened, adjust the index.
      result = true; // We actually removed someone.
    }
  }
  return result;
}

function onNewSingle(msg, connection) {
  if (msg.username === undefined) {
    return { type: 'error', msg: 'Need "username" field.' };
  }
  var new_user = addSingle(msg, connection);
  var num_singles = arena.single_pool.length;
  if (num_singles <= 1) {
    return {type: 'wait', msg: 'Please wait'};
  }

  var suggestion;
  do {
    var random_idx = getRandomIdx(num_singles);
    suggestion = arena.single_pool[random_idx];
  } while (new_user.fingerprint === suggestion.fingerprint);

  // Send suggestion to other guy.
  var ret_msg = {type: 'partner_suggestion',
                 partner_suggestion: msg.username};
  suggestion.connection.write(JSON.stringify(ret_msg));

  // Return suggestion to current guy.
  ret_msg.partner_suggestion = suggestion.username;
  return ret_msg;
}

function onWhoTeam(data, connection) {
  var teams = arena.team_pool.map(function(team){ return team.usernames; });
  return {type: 'debug', teams: teams};
}

function onWhoSingle(data, connection) {
  var singles = arena.single_pool.map(function(single) {
    return single.username;
  });
  return {type: 'debug', singles: singles};
}

function onUnseekSingle(data, connection) {
  if (removeSingle(connection)) {
    return {type: 'success', msg: 'User has been removed from singles pool.'};
  }
  return {type: 'error', msg: 'User was not in singles pool.'};
}

function onUnseekTeam(data, connection) {
  if (removeTeam(connection)) {
    return {type: 'success', msg: 'User has been removed from team pool.'};
  }
  return {type: 'error', msg: 'User was not in team pool.'};
}

function onNewMsg(data, connection) {
  try {
    var msg = JSON.parse(data);
  } catch (err) {
    return {type: 'error', msg: 'Could not JSON parse msg'};
  }

  if (msg.type === undefined) {
    return {type: 'error', msg: 'We need "type".'};
  }

  var event_handler = arena_event_handlers[msg.type];
  if (event_handler === undefined) {
    return {type: 'error', msg: 'Arena msg type not supported.'};
  }
  return event_handler(msg, connection);
}

function onLostConnection(connection) {
  removeSingle(connection);
  removeTeam(connection);
}

//-----------------------------------------------------------------------------
// Frontend. Starts services on a couple ports.
//-----------------------------------------------------------------------------

function startTestService() {
  net.createServer(function(socket) {
    socket.on('data', function(data) {
      socket.write(JSON.stringify(onNewMsg(data, socket)));
    });
    socket.on('close', function() { onLostConnection(socket); });
    socket.on('error', function() { onLostConnection(socket); });
  })
  .listen(ports.test);
}

function startProductionService() {
  var arena_bot = sockjs.createServer();
  arena_bot.on('connection', function(conn){
    conn.on('data', function(data) {
      socket.write(JSON.stringify(onNewMsg(data, conn)));
    });
    conn.on('close', function() { onLostConnection(conn); });
    conn.on('error', function() { onLostConnection(conn); });
  });

  var server = http.createServer();
  arena_bot.installHandlers(server, {prefix: '/arena_bot'});
  server.listen(ports.production, '0.0.0.0');
}

startProductionService();
startTestService();

console.log('Arena bot started. Listening on ports: ');
console.log(ports);
