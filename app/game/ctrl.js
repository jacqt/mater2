/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 * A controller for logging in to the server
 */
(function(){
    angular.module('mater2.controllers').controller('GameCtrl',
        ['$scope', 'GameService', controller])
    function controller($scope, GameService){
        $scope.boards = GameService.boards;
        $scope.PieceMoved = GameService.PieceMoved;
        setTimeout(function(){
          $scope.$apply();
        }, 10);
    }
})();
