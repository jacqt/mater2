/**
 * Service for logins, instead of controller.
 * If controller, then multiple invocations would
 * disrupt invariants. For instance, multiple
 * listeners might get added.
 */

(function(){
    angular.module('mater2.services').service('GameService',
        ['Timeseal', 'IcsService', 'ProfileService', '$rootScope',
    function(Timeseal, IcsService, ProfileService, $rootScope) {

        var _boards = {};
        var _my_game;

        //---------------------------------------------------------------------
        // Helper functions.

        // Need to convert the board received from the the server
        // into a format the angularjs chessboard recognizes.
        //
        // FEN format follows these rules:
        // o A number represents the number of consecutive empty squares
        //   in a row.
        // o Rows are delimited by "/"
        // o Black pieces are lower-case.
        // o White pieces are upper-case.
        //
        // Examples of valid FEN positions:
        // o Ruy Lopez:
        //    'r1bqkbnr/pppp1ppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R'
        //
        function ConvertToFen(ics_position) {
            // Each row must be delimited by "/".
            new_position = ics_position.trim().replace(/ /g, '/');

            // Initialize the reduce operation by replacing each
            // "-" with a "1".
            new_position = new_position.replace(/-/g, '1');

            // Reduce the number of consecutive "1"'s to their sum.
            return new_position.split("").reduce(function(a, b) {
                /* Examples:
                 * o ("1", "1") => "2".
                 * o ("/", "1") => "/1"
                 * o ("1", "/") => "1/"
                 * o ("2", "1") => "3"
                 * o ("1", "r") => "1r"
                 */
                left_num = parseInt(a.charAt(a.length-1), 10);
                right_num = parseInt(b.charAt(0), 10);
                if (left_num && right_num) {
                    var new_value = (left_num+right_num).toString();
                    return (a.substr(0, a.length-1) + new_value + b.substr(1));
                } else {
                    return (a + b); // String concat, not arithmetic addition.
                }
            }, "")
        }

        function IsMyGame(game_state) {
            var username = ProfileService.data.username;
            var white, black;
            if (game_state.white !== undefined) {
                white = game_state.white;
                black = game_state.black;
            } else if (game_state.white_player !== undefined) {
                white = game_state.white_player;
                black = game_state.black_player;
            } else {
                console.error("game_state did not contain player info.");
            }

            return (white.name === username || black.name === username);
        }

        function SetTopBottomPlayers(game_state) {
          console.log(game_state.black);
          console.log(ProfileService.data.partner);

            if (game_state.flip === 1 ||
                game_state.black.name === ProfileService.data.partner) {
                game_state.flip = 1;
                game_state.top_player = game_state.white;
                game_state.top_player.color = 'W';

                game_state.bottom_player = game_state.black;
                game_state.bottom_player.color = 'B';
            } if (game_state.flip === 0) {
                game_state.top_player = game_state.black;
                game_state.top_player.color = 'B';

                game_state.bottom_player = game_state.white;
                game_state.bottom_player.color = 'W';
            }
        }

        function UpdateBoard(game_state) {
            // Manually go through and set attributes instead of simply
            // setting the object. So that previously added attributes
            // do not get clobbered.

            var game_id = game_state.game_id;

            // Initialize game obj.
            if (_boards[game_id] === undefined) {
                _boards[game_id] = {};
            }

            // Game Id.
            if (game_id !== undefined) {
                _boards[game_id].game_id = game_id;
            } else {
                console.error("`game_id` is undefined.");
            }

            // Initialize player objects.
            function InitializePlayerObj(color) {
                if (_boards[game_id][color] === undefined) {
                    _boards[game_id][color] = {};
                }
            }
            InitializePlayerObj("white");
            InitializePlayerObj("black");

            // Position
            if (game_state.position !== undefined) {
                _boards[game_state.game_id].position = game_state.position;
            }

            // Chess variant.
            if (game_state.chess_variant !== undefined) {
                var chess_variant = game_state.chess_variant;

                _boards[game_id].chess_variant = chess_variant;
                if (chess_variant === "crazyhouse" ||
                    chess_variant === "bughouse") {
                    if (_boards[game_id].white.pieces_in_hand === undefined) {
                        _boards[game_id].white.pieces_in_hand = [];
                    }
                    if (_boards[game_id].black.pieces_in_hand === undefined) {
                        _boards[game_id].black.pieces_in_hand = [];
                    }
                } else {
                    _boards[game_id].white.pieces_in_hand = undefined;
                    _boards[game_id].black.pieces_in_hand = undefined;
                }
            }

            // Player's information.
            function SetPlayerInfo(player, color) {
                if (player === undefined) {
                    return;
                }
                if (player.rating !== undefined) {
                    _boards[game_id][color].rating = player.rating;
                }
                if (player.name !== undefined) {
                    _boards[game_id][color].name = player.name;
                }
                if (player.time_ms !== undefined) {
                    _boards[game_id][color].time_ms = player.time_ms;
                }
            }
            SetPlayerInfo(game_state.white_player, "white");
            SetPlayerInfo(game_state.black_player, "black");

            // Flip.
            if (game_state.flip !== undefined) {
                _boards[game_id].flip = game_state.flip;
            }

            // Lag.
            if (game_state.lag !== undefined) {
                _boards[game_id].lag = game_state.lag;
            }

            // Clock is ticking.
            if (game_state.clock_is_ticking !== undefined) {
                _boards[game_id].clock_is_ticking = game_state.clock_is_ticking;
            }

            // Turn.
            if (game_state.turn !== undefined) {
                _boards[game_id].turn = game_state.turn;
            }

            // Game State Checkpoint.
            if (game_state.checkpoint !== undefined) {
                _boards[game_id].checkpoint = game_state.checkpoint;
            }

            // Set top and bottom players. Important that this happens
            // after 'white' and 'black' data has been set.
            SetTopBottomPlayers(_boards[game_id]);

            if (IsMyGame(_boards[game_id])) {
                SetMyGame(_boards[game_id]);
            }
        }

        function SetMyGame(game_state) {
            _my_game = _boards[game_state.game_id];
        }

        // Saves the current game state into "checkpoint"
        // attribute.
        function CheckpointGameState(game_state) {
            console.log(game_state); // XXX.
            if (game_state.checkpoint === undefined) {
                game_state.checkpoint = {};
                game_state.checkpoint.white = {};
                game_state.checkpoint.black = {};
            }
            if (game_state.position !== undefined) {
                game_state.checkpoint.position = game_state.position;
            }
            if (game_state.white !== undefined &&
                game_state.white.pieces_in_hand !== undefined) {
                game_state.checkpoint.white.hand = game_state.white.pieces_in_hand;
            }
            if (game_state.black !== undefined &&
                game_state.black.pieces_in_hand !== undefined) {
                game_state.checkpoint.black.hand = game_state.black.pieces_in_hand;
            }
        }

        function LoadGameStateCheckpoint(game_state) {
            setTimeout(function(){
              game_state.position = game_state.checkpoint.position;
              game_state.white.pieces_in_hand = game_state.checkpoint.white.hand;
              game_state.black.pieces_in_hand = game_state.checkpoint.black.hand;

              // This is such an ugly hack. At least it works.
              // FIXME: figure out some better way to get things updated.
              $rootScope.$apply();
            }, 100);
        }

        function StopClocks(game_state) {
            game_state.clock_is_ticking = 0;
        }

        //---------------------------------------------------------------------
        // Callbacks for IcsService events.

        function GameStarted(response) {
            // Clear out all the boards when a game starts.
            for (var board_id in _boards) {
                delete _boards[board_id];
            }

            var new_game = response.parsed;
            UpdateBoard(new_game);
        }

        // Everytime a new board is received from the server,
        // the updated game state will be the new checkpoint.
        function BoardUpdated(response) {
            // TODO: document what the board will look like.
            var new_state = response.parsed;

            new_state.position = ConvertToFen(new_state.position);
            if (IsMyGame(new_state)) {
                CheckpointGameState(new_state);
            }
            UpdateBoard(new_state);
        }

        function NotMyTurn() {
            LoadGameStateCheckpoint(_my_game);
        }

        function IllegalMove(response) {
            LoadGameStateCheckpoint(_my_game);
        }

        function PiecesInHandUpdated(response) {
            var new_state = response.parsed;
            var game_id = new_state.game_id;
            var white_hand = new_state.white_is_holding.split("");
            var black_hand = new_state.black_is_holding.split("");
            _boards[game_id].white.pieces_in_hand = white_hand;
            _boards[game_id].black.pieces_in_hand = black_hand;
            CheckpointGameState(_boards[game_id]);
            UpdateBoard(_boards[game_id]);
        }

        function GameEnded(response) {
            var game_result = response.parsed;
            var game_state = _boards[game_result.game_id]
            game_state.game_end_msg = game_result.reason;
            StopClocks(game_state);
            UpdateBoard(game_state)
        }

        function Pinged() {
            IcsService.SendLine(Timeseal.PingReply);
        }

        IcsService.AddListener("pinged", Pinged);
        IcsService.AddListener("board updated", BoardUpdated);
        IcsService.AddListener("pieces-in-hand updated", PiecesInHandUpdated);
        IcsService.AddListener("illegal move", IllegalMove);
        IcsService.AddListener("not my turn", NotMyTurn);
        IcsService.AddListener("game over", GameEnded);
        IcsService.AddListener("game started", GameStarted);

        //---------------------------------------------------------------------
        // Public API.

        /**
         * "from_square" and "to_square" are <string>'s
         * in algebraic notation, e.g. "e2" or "f4".
         *
         * "position_after" is an object describing the piece
         * on each square.
         */
        function PieceMoved(from_square, to_square, moved_piece,
                            position_after, position_before) {
            if (from_square === to_square) { return; }
            if (to_square === "offboard") {
                LoadGameStateCheckpoint(_my_game);
                return;
            }

            _my_game.position = ChessBoard.objToFen(position_after);
            UpdateBoard(_my_game);

            if (from_square === "spare") {
                var notation_of_piece = moved_piece.charAt(1);
                from_square = notation_of_piece + "@";
            }

            var move = from_square + to_square;
            IcsService.SendLine(move);
        }

        return {
          boards: _boards,

          // Callbacks.
          PieceMoved: PieceMoved
        }
    }]);
})();
