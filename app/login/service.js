/**
 * Service for logins, instead of controller.
 * If controller, then multiple invocations would
 * disrupt invariants. For instance, multiple
 * listeners might get added.
 */

(function(){
    angular.module('mater2.services').service('LoginService',
        ['$cookies', 'IcsService', 'ProfileService',
    function($cookies, IcsService, ProfileService) {

        var _login_combination = {
            username: "",
            password: "",
        };

        var _error = {msg: ""};
        var _status = {msg: "", logged_in: false};

        //---------------------------------------------------------------------
        // Callbacks for IcsService events.

        function UsernameRequested() {
            IcsService.SendLine(_login_combination.username);
        }

        function PasswordRequested() {
            IcsService.SendLine(_login_combination.password);
        }

        function SessionStarted(response) {
            ProfileService.data.username = response.parsed.username;
            _status.msg = "Success!";
            _status.logged_in = true;

            // Subscribe to channel 24 on login.
            IcsService.SendLine('+ch 24');

            // Set cookies remembering the user on this computer.
            if (_login_combination.username !== "guest") {
                $cookies.put("BHC-Auth-User", _login_combination.username);
                $cookies.put("BHC-Auth-Password", _login_combination.password);
            }
        }

        function PasswordInvalid() {
            ThrowError("Password invalid.");
        }

        function UsernameNotFound() {
            if (_login_combination.username !== 'guest'){
                ThrowError("Username not found.");
            }
        }

        function UsernameNonAlpha() {
            ThrowError("Username can only have upper and lower case.");
        }

        // Listen for session-related events.
        IcsService.AddListener("username?", UsernameRequested);
        IcsService.AddListener("password?", PasswordRequested);
        IcsService.AddListener("password invalid!", PasswordInvalid);
        IcsService.AddListener("username nonalpha!", UsernameNonAlpha);
        IcsService.AddListener("username not found!", UsernameNotFound);
        IcsService.AddListener("session started", SessionStarted);

        //---------------------------------------------------------------------
        // Public API.

        function GuestLogin() {
            IcsService.AddListener("username not found!", UsernameNotFoundGuest,
                                   true /* invoke callback only once. */);

            // Login as guest first.
            Login({username: 'guest', password: ' '});

            function UsernameNotFoundGuest(){
                IcsService.SendLine("\r");
            }
        }

        function Reset() {
            _login_combination.username = "";
            _login_combination.password = "";
            _error.msg = "";
            _status.msg = "";
            _status.logged_in = false;
        }

        function Login(data) {
            _login_combination = data;
            IcsService.Close();
            _status.msg = "Logging in...";
            _status.logged_in = false;
            _error.msg = "";
            IcsService.NewConnection();
            // Once a new connection been started,
            // the server will request a username, -> UsernameRequested().
        }

        function ThrowError(msg) {
            IcsService.Close();
            _status.msg = "";
            _status.logged_in = false;
            _error.msg = msg;
        }

        function LoginDefaultSettings() {
            // TODO: constant away the following two magic words.
            var user = $cookies.get("BHC-Auth-User");
            var password = $cookies.get("BHC-Auth-Password");

            if (user !== undefined && password !== undefined) {
                Login({"username": user, "password": password});
                return;
            }
            GuestLogin();
        }

        return {
            // Logins using default settings. By default settings, we mean
            // to either use the credentials stored in the user's cookie,
            // or otherwise login as guest.
            LoginDefaultSettings: LoginDefaultSettings,

            // Logins as a guest.
            GuestLogin: GuestLogin,

            // Attempt login to the server.
            Login: Login,

            // Reset variables. Could be used by controllers
            // when controllers are invoked multiple times.
            Reset: Reset,

            // Throw error is public to let controllers do validation.
            ThrowError: ThrowError,

            // Controllers can bind to these objects.
            error: _error,
            status: _status,
        }
    }]);
})();
