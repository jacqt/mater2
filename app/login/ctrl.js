/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 * A controller for logging in to the server
 */
(function(){
    angular.module('mater2.controllers').controller('LoginCtrl',
        ['$scope', 'LoginService', 'LocalizationService', '$modalInstance', controller])
    function controller($scope, LoginService, LocalizationService, $modalInstance){
        $scope.locale = LocalizationService.Locale;
        // Need to make 'username' and 'password' child elements of input.
        // The issue is documented here:
        // http://stackoverflow.com/questions/12618342/ng-model-does-not-update-controller-value
        $scope.input = {
            username: '',
            password: '',
        };

        // Listen to LoginService's properties to help
        // display useful information.
        $scope.error = LoginService.error;
        $scope.login_status = LoginService.status;

        // Dirty data could have been
        // kept across multiple invocations.
        LoginService.Reset();

        function invalidInput(data){
            return (data.username === 'guest' 
                    || data.username === '' 
                    || data.password === '');
        }

        $scope.AttemptLogin = function() {
            if (invalidInput($scope.input)){
                LoginService.ThrowError('Invalid username or password');
            } else {
                LoginService.Login($scope.input);
            }
        }

        $scope.onKeyPressPwd = function(e){
            if (e.keyCode == 13){
                $scope.AttemptLogin();
            }
        }

        $scope.Close = function() {
            $modalInstance.close({ status: 'success' });
        }
    }
})();
