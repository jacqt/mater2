/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 * Service for displaying information about the user
 */

(function(){
    angular.module('mater2.services').service('ProfileService',
        [service]);

    function service() {
        var _data = {
            username: "",
            partner: ""
        };

        function GetUsername() {
            return _data.username;
        }

        return {
            data: _data,

            // Returns immutable string of the player's username.
            GetUsername: GetUsername
        };
    }
})();
