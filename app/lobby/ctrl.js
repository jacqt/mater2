/** Alex Guo (chessnut@outlook.com)
 * A controller for displaying lobby data.
 */
(function(){
    angular.module('mater2.controllers').controller('LobbyCtrl',
        ['$scope', 'LobbyService', 'ImcService', 'LocalizationService', 'PartnerService', controller])
    function controller($scope, LobbyService, ImcService, LocalizationService, PartnerService ){
        $scope.locale = LocalizationService.Locale;
        $scope.info = LobbyService.info;
        
        var name_parser = /[^A-Za-z]*([A-Za-z]*)[^A-Za-z]*/;

        $scope.chat_with = function(player){
            var name = name_parser.exec(player)[1];
            ImcService.fireEvent('open chat group', {
                name: name
            });
        }

        $scope.offer_partnership = function (player) {
            var name = name_parser.exec(player)[1];
            PartnerService.OfferPartner(name);
        }
        $scope.match_player = function (player) {
            var name = name_parser.exec(player)[1];
            ImcService.fireEvent('make match offer', {
                name: name
            });
        }
    }
})();
