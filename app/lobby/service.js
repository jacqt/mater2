/**
 * Service to display information about people in the lobby.
 * o Partnerships.
 * o Games.
 * o Seeks.
 */

(function(){
    angular.module('mater2.services').service('LobbyService',
        ['IcsService', function(IcsService) {

        var _info = {
            bug_games: [],
            partnerships: [],
            all_players: []
        };

        function ReceivedLobbyInfo(response) {
            _info.bug_games.length = 0;
            _info.partnerships.length = 0;
            _info.all_players.length = 0;
            lobby_data = JSON.parse(response.data);
            _info.bug_games = lobby_data.curBugGames;
            _info.partnerships =lobby_data.curPartners;
            //_info.all_players = lobby_data.allPlayers;
            _info.all_players = lobby_data.allPlayers.map(function(player){
                return /[^A-Za-z]*([A-Za-z]*)[^A-Za-z]*/.exec(player)[1];
            });
        }

        IcsService.AddListener("lobby info", ReceivedLobbyInfo);

        return {
            info: _info
        }
    }]);
})();
