(function(){
    angular.module('mater2.services').service('NotifsService',
        ['IcsService', 'ProfileService', 'LocalizationService',
        function(IcsService, ProfileService, LocalizationService) {
        var challengedRe = /Challenge: ([A-Za-z]+) (\(.*?\))* ([A-Za-z]+) (\(.*?\))* ([A-Za-z]*) ([A-Za-z]*) ([0-9]*) ([0-9]*).*/;
        var partnerReqRe = /[A-Za-z]+ offers to be your bughouse partner.*/;
        var updateRe = [challengedRe, partnerReqRe];

        var pendingReadChallengeRe = / ([0-9]*):.* challenge: ([A-Za-z]+) (\(.*?\))* ([A-Za-z]+) (\(.*?\))* ([A-Za-z]*) ([A-Za-z]*) ([0-9]*) ([0-9]*).*/;
        var pendingReadPartnerReqRe = / ([0-9]*): ([A-Za-z]+) is offering to be bughouse partners.*/; 

        var _numberOfNewNotifications = 0;
        var _offers = [];
        var _notifications = [];

        function Accept(notif){
            console.log(notif);
            IcsService.SendLine('accept ' + notif.offerNumber);
            IcsService.SendLine('pending');
            //_offers.splice(_offers.indexOf(notif),1);
        }

        function Dismiss(notif){
            console.log(notif);
            if (notif.offerNumber){ // Notif is an offer
                IcsService.SendLine('decline ' + notif.offerNumber);
                IcsService.SendLine('pending');
            }
            else { // Notif is a notification
                _notifications.splice(_notifications.indexOf(notif), 1);
            }
        }

        function onMessage(data){
            var currentMode = null;
            var lines = data.data.split('\n');
            for (var i = 0; i != lines.length; ++i){
                var line = lines[i];
                if (currentMode == 'readingOffers'){
                    console.log(_offers);
                    var m = pendingReadChallengeRe.exec(line);
                    if (m != null){
                        var offerNumber = m[1];
                        var challengeIssuer = m[2];
                        var challengeIssuerRating = m[3];
                        var challengeRecipient = m[4];
                        var challengeRecipientRating = m[5];
                        var ratedStr = m[6]
                        var variant = m[7];
                        var time = parseInt(m[8])/60;
                        var inc = m[9];
                        var offerObj = {
                            title: challengeIssuer + ' ' + challengeIssuerRating,
                            details: ratedStr + ' ' + variant + ' ' + time + ' ' + inc,
                            offerNumber: offerNumber,
                            canAccept: true
                        };
                        _offers.push(offerObj);
                    }

                    m = pendingReadPartnerReqRe.exec(line);
                    if (m != null){
                        var offerNumber = m[1];
                        var offerMaker = m[2];
                        var offerObj = {
                            title: offerMaker + ' offers to be bughouse partners',
                            offerNumber: offerNumber,
                            canAccept: true
                        };
                        _offers.push(offerObj);
                    }
                } else {
                    for (var j = 0; j != updateRe.length; ++j){
                        var re = updateRe[j];
                        var m = re.exec(line);
                        if (m != null){
                            IcsService.SendLine('pending');
                            _numberOfNewNotifications += 1;
                            break;
                        }
                    }

                    m = /Offers FROM other players:.*/.exec(line);
                    if (m != null){
                        currentMode = 'readingOffers';
                        _offers.splice(0, _offers.length); 
                    }
                    m = /There are no offers pending FROM other players.*/.exec(line);
                    if (m != null){
                        _offers.splice(0, _offers.length); 
                    }
                }
            }
        }

        function OnPartnershipSuccessful(data){
            var notifObj = {
                title: LocalizationService.Locale.successfully_partnered + ' ' + data.parsed.partner_name,
                details: '',
                canAccept: false,
            };
            _notifications.push(notifObj);

            var partner_name = data.parsed.partner_name;

            // Follow your partner
            IcsService.SendLine('pfollow ' + partner_name);

            // Push data to the ProfileService.
            ProfileService.data.partner = partner_name;
        }

        function OnPartnershipDeclined(data){
            var notifObj = {
                title: data.parsed.partner_name + ' ' + LocalizationService.Locale.declined_your_partnership_request,
                details: '',
                canAccept: false,
            };
            _notifications.push(notifObj);
        }

        IcsService.AddListener('all', onMessage);
        IcsService.AddListener('accept partnership successful', OnPartnershipSuccessful);
        IcsService.AddListener('parternship request accepted', OnPartnershipSuccessful);
        IcsService.AddListener('partnership request declined', OnPartnershipDeclined);
        return {
            Accept : Accept,
            Dismiss: Dismiss,
            offers : _offers,
            notifications: _notifications
        }
    }]);
})();
