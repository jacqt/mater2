/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 * Controller for a notifications dropdown
 */

(function(){
    angular.module('mater2.controllers')
        .controller('NotifsCtrl',
            ['$scope', 'NotifsService', 'LocalizationService', controller]);

    function controller($scope, NotifsService, LocalizationService){
        $scope.locale = LocalizationService.Locale;
        $scope.notifications = NotifsService.notifications;
        $scope.accept = NotifsService.Accept;
        $scope.dismiss = NotifsService.Dismiss;
        $scope.offers = NotifsService.offers;
    }

})();

