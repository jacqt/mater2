/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 * A controller for offering a match.
 */
(function () {
    angular.module('mater2.controllers').controller('MatchCtrl',
        ['$scope', 'MatchService', 'LocalizationService', 'LobbyService', 'ImcService', controller])
    function controller($scope, MatchService, LocalizationService, LobbyService, ImcService) {
        var self = this;

        $scope.locale = LocalizationService.Locale;
        // Need to make child elements of input. The issue is documented here:
        // http://stackoverflow.com/questions/12618342/ng-model-does-not-update-controller-value
        MatchService.Reset()
        $scope.input = {
            username: '',
            item: null
        };

        $scope.searchText = null;
        $scope.selectedItem = null;

        $scope.info = LobbyService.info;

        $scope.all_players = [];
        $scope.$watch('info.all_players', function (newVal) {
            $scope.all_players = $scope.info.all_players.map(function (p) {
                return {
                    value: p.toLowerCase(),
                    display: p
                }
            })
        });


        $scope.querySearch = function (query) {
            var results = query ? $scope.all_players.filter(createFilterFor(query)) : [];
            return results;
        }

        $scope.variants = [
            { name: $scope.locale.chess, value: ' ' },
            { name: $scope.locale.bughouse, value: 'bughouse' },
            { name: $scope.locale.crazyhouse, value: 'zh' }
        ];

        $scope.choice = {
            variant: ' ',
            time: null,
            increment: null,
        };

        $scope.match_issued = MatchService.match_issued;

        $scope.error = MatchService.error;

        $scope.OfferMatch = function () {
            MatchService.OfferMatch({
                matchTo: $scope.input.username,
                variant: $scope.choice.variant,
                time: $scope.choice.time,
                increment: $scope.choice.increment
            });
        }
        ImcService.addHandler("make match offer", function(obj){
          console.log(obj);
          $scope.input.username = obj.name;
        });

        /**
         * Create filter function for a query string 
         *
         */
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(player) {
                return (player.value.indexOf(lowercaseQuery) === 0);
            };
        }
    }
})();
