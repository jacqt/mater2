angular.module('mater2.controllers')
.controller('gameSettingSelectorController', ['$scope', 'LocalizationService', function($scope, LocalizationService){
    $scope.locale = LocalizationService.Locale;
}])
.directive('gameSettingSelector', function(){
    return {
        restrict: 'AEC',
        templateUrl: 'app/match/game_setting_selector.html'
    }
});
