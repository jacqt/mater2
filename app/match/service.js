/**
 * Service for offering matches.
 */

(function(){
    angular.module('mater2.services').service('MatchService',
        ['IcsService', 'ImcService', 'toaster', function(IcsService, ImcService, toaster) {
            var _error = {
                msg: ''
            };

            var _match_issued = {
                status: false
            };
            function OfferMatch(choice){
                var username = choice.matchTo;
                var cmd = ['match', choice.matchTo, choice.variant, choice.time, choice.increment ];

                IcsService.SendLine(cmd.join(' '));
            }

            function stripFicsString(s){
                return /(.*)\n/.exec(s)[1];
            }

            function MatchError (response){
                ThrowError(stripFicsString(response.data));
            }

            function MatchIssued(response){
                Reset();
                _match_issued.status = true;

                toaster.pop('success', 'Successfully issued match request');
            }

            function ThrowError(msg) {
                _error.msg = msg;
                toaster.pop('error', msg);
            }

            function Reset(){
                _error.msg = '';
                _match_issued.status = false;
            }

            IcsService.AddListener("match error message", MatchError);
            IcsService.AddListener("match issued", MatchIssued);
        
            return {
                OfferMatch : OfferMatch,
                match_issued : _match_issued,
                error : _error,
                Reset: Reset
            };
    }]);
})();
