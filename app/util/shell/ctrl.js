/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 *
 * A shell to interact with the bughouse internet chess server.
 *
 * The shell is only turned on if the "dev=true" flag is in
 * the URL.
 *
 */
(function(){
    angular.module('mater2.controllers').controller('IcsShell',
    ['$scope','ConfigService', 'IcsService', function($scope, ConfigService, IcsService) {

        $scope.screen = '';

        // Need to make 'cmd' a child element of input. The issue is
        // documented here:
        // http://stackoverflow.com/questions/12618342/ng-model-does-not-update-controller-value
        $scope.input = {cmd: ''};
        $scope.in_dev_mode = ConfigService.InDevMode();

        $scope.submit = function(cmd){
            IcsService.SendLine(cmd);
            $scope.input.cmd = '';
        }

        function MsgArrived(response) {
            $scope.screen += response.data;
        }

        IcsService.AddListener("all", MsgArrived);
    }]);
})();
