/** Intermodule communicator service 
 */
(function(){
    var eventToCallbackMap = {};
    angular.module('mater2.services').service('ImcService', function(){ 
        return {
            addHandler: function(eventName, callback){
                if (eventToCallbackMap[eventName] == null){
                    eventToCallbackMap[eventName] = [callback];
                } else {
                    eventToCallbackMap[eventName].push(callback);
                }
            },
            fireEvent : function(eventName, data){
                var callbacks = eventToCallbackMap[eventName];
                if (callbacks == null){
                    return;
                }
                for (var i = 0; i != callbacks.length; ++i){
                    var callback = callbacks[i];
                    callback(data);
                }
            }
        }
    });
})();
