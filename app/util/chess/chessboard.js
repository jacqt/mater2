(function () {
  'use strict';

  angular.module('ng.chessboard', [])

  .directive('ngChessboard', [
    '$window',
    '$log',
    '$timeout',
    function($window, $log, $timeout) {
      return {
        restrict: 'EA',
        scope: {
          position: '=',
          sparePieces: '=',

          // callbacks
          onChange:'&',
          onDragMove: '&',
          onDrop: '&',
          onSnapbackEnd: '&',
          onMoveEnd: '&',
          onMouseoutSquare: '&',
          onMouseoverSquare: '&',
          onSnapEnd: '&',
          // workaround because angular does not like 'onDragStart'
          onDragStart:'&onDragStartCb',

          // Added config options by BughouseClub.
          flip: '=',
          whiteHand: '=',
          blackHand: '='
        },
        priority: 1000,
        template: '<div></div>',

        link: function link($scope, $element, $attrs, $ctrl) {
          var board_config = {
            // These are default values.
            position: undefined,
            showNotation: true,
            orientation: 'white',
            draggable: true,
            dropOffBoard: 'snapback',
            appearSpeed: 200,
            moveSpeed: 200,
            snapbackSpeed: 50,
            snapSpeed: 25,
            trashSpeed: 100,
            sparePieces: false,
            showErrors: false,
            pieceTheme: undefined, // defaults to 'img/chesspieces/wikipedia/{piece}.png'
          };

          var board_element = angular.element('<div></div>');

          var callback_options = [
            'onChange',
            'onDragStart',
            'onDragMove',
            'onDrop',
            'onSnapbackEnd',
            'onMoveEnd',
            'onMouseoutSquare',
            'onMouseoverSquare',
            'onSnapEnd',
          ];

          // These are configurable options in ChessBoard.
          var normal_config_options = [
            'draggable',
            'dropOffBoard',
            'position',
            'orientation',
            'showNotation',
            'sparePieces',
            'showErrors',
            'pieceTheme',
            'appearSpeed',
            'moveSpeed',
            'snapbackSpeed',
            'snapSpeed',
            'trashSpeed',
          ];

          // These are configurable options that BughouseClub
          // has added.
          //
          // TODO: make chessboardjs understand these added config options.
          var added_config_options = [
            'flip',
          ];

          function ConfigureBoard() {
            angular.forEach(normal_config_options, function(option) {
              if ($scope[option] !== undefined) {
                board_config[option] = $scope[option];
              } else if ($attrs[option]) {
                board_config[option] = $attrs[option];
              }
            });

            angular.forEach(callback_options, function(option) {
              // $scope[option] is actually a wrapper around the actual fn.
              // By calling the wrapper with "()", we get the original fn as a
              // first-class variable.
              var unwrapped_callback = $scope[option]();
              if (unwrapped_callback) {
                board_config[option] = unwrapped_callback;
              }
            });

            angular.forEach(added_config_options, function(option) {
              if ($scope[option] !== undefined) {
                board_config[option] = $scope[option];
              } else if ($attrs[option]) {
                board_config[option] = $attrs[option];
              }
            });
          }

          function SetOrientation(flip) {
            if (flip) {
              $scope.board.orientation('black');
            } else {
              $scope.board.orientation('white');
            }
          }

          function CreateBoard() {
            $element.prepend(board_element);
            $scope.board = new $window.ChessBoard(board_element, board_config);

            SetOrientation(board_config['flip']);
          }

          function DestroyBoard() {
            $scope.board.destroy();
          }

          function OnDestruction() {
            $scope.$on('$destroy', DestroyBoard);
          }

          function OnPositionChg() {
            $scope.$watch('position', function(val) {
              // The 'true' argument forces animation, which makes
              // a piece's old square to become vacant.
              $scope.board.position(val, true);
            }, true);
          }

          function OnColorChg() {
            $scope.$watch('flip', function(flip) {
              SetOrientation(flip);
            }, true);
          }

          function OnPlayersHandChg() {
            var updateWhiteHand, updateBlackHand;

            // CSS selectors for the top/bottom spare pieces.
            var TOPHAND_SELECTOR = '.spare-pieces-top-4028b';
            var BOTTOMHAND_SELECTOR = '.spare-pieces-bottom-ae20f'
            var IMG_TAG = "img";

            function numInHand(piece, currently_holding) {
              var num = 0;
              for (var i = 0; i < currently_holding.length; i++) {
                if (currently_holding[i] === piece) {
                  num++;
                }
              }
              return num;
            }

            function updateHand(selector, newHand) {
              function getPiece(spare_piece_dom_elem) {
                var notation_of_piece = spare_pieces[i].dataset.piece;
                if (notation_of_piece === undefined) {
                  return undefined;
                } else {
                  notation_of_piece = notation_of_piece.charAt(1);
                }
                return notation_of_piece;
              }

              var spare_pieces = $element.find(selector).find(IMG_TAG);
              for (var i = 0; i < spare_pieces.length; i++) {
                var notation_of_piece = getPiece(spare_pieces[i]);
                if (notation_of_piece === undefined) { continue; }
                var dom_id = spare_pieces[i].id;
                var num_pieces = numInHand(notation_of_piece, newHand);
                if (num_pieces > 0) {
                  $element.find('#' + dom_id).css("visibility", "visible");
                } else {
                  $element.find('#' + dom_id).css("visibility", "hidden");
                }
              }
            }

            function updateTopHand(newHand) {
              updateHand(TOPHAND_SELECTOR, newHand);
            }
            function updateBottomHand(newHand) {
              updateHand(BOTTOMHAND_SELECTOR, newHand);
            }

            updateBlackHand = updateTopHand;
            updateWhiteHand = updateBottomHand;
            if ($scope.flip) {
              updateWhiteHand = updateTopHand;
              updateBlackHand = updateBottomHand;
            }

            $scope.$watch('whiteHand', updateWhiteHand);
            $scope.$watch('blackHand', updateBlackHand);
          }

          function InstallObservers() {
            OnDestruction();
            OnPositionChg();
            OnColorChg();
            OnPlayersHandChg();
          }

          ConfigureBoard();
          CreateBoard();

          // Install observers AFTER the board has been created.
          // InstallObservers() depends on the $element being
          // fleshed out, as it will modify spare pieces info.
          InstallObservers();
        },
      };
    }
  ])
})();
