/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 *
 * Simple adapter to connect to the server.
 *
 * FIXME: Possible memory leak as eventhandlers are never 
 * deregistered 
 */
(function(){
    // The IcsAdapter implements the timeseal protocol on top
    // of a websocket.
    angular.module('mater2.services').service('IcsAdapter',
    ['Timeseal', 'Websocket', function (Timeseal, Websocket) {

        // The WEBSOCKET_PROXY sits between the web client
        // and the actual bughouse server.
        var WEBSOCKET_PROXY = "http://bughouseclub.com:5011";

        var _websocket = new Websocket();

        /* Each object in listeners is expected to be:
         * {
         *   callback: <callback_fn>,
         * }
         */
        var listeners = [];

        function SendLine(line) {
            console.log("SENDING LINE: " + line);
            console.assert(line[line.length - 1] !== '\n');

            _websocket.SendLine(Timeseal.Encode(line));
        }

        function NewConnection() {
            _websocket.Connect(WEBSOCKET_PROXY);
            _websocket.SetHandler('message', NotifyListeners);
        }

        function NotifyListeners(response) {
            console.log("RECEIVED :>" + response.data);
            for (var i in listeners) {
                listeners[i].callback(response);
            }
        }

        function AddListener(callback) {
            listeners.push({callback: callback});
        }

        function Close() {
            _websocket.Close();
        }

        return {
            SendLine: SendLine,

            // The callback will be invoked on any incoming message.
            // The callback will be passed a new message.
            AddListener: AddListener,

            // Create a new connection to bughouseclub.com.
            // Throws away the old connection.
            NewConnection: NewConnection,

            // Close the websocket.
            Close: Close,
        };
    }]);
})();
