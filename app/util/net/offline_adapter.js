/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 *
 * Simple adapter to receive information that does not require a session.
 *
 * FIXME: Possible memory leak as eventhandlers are never 
 * deregistered 
 */
(function(){
    // The OfflineAdapter interprets communication that does
    // not require a server session.
    angular.module('mater2.services').service('OfflineAdapter',
    ['Websocket', function (Websocket) {

        // The server sitting on port 3000 pushes information
        // to this adapter.
        var SERVER_API = "http://bughouseclub.com:3000/stream";

        // Instantiate a new websocket
        _websocket = new Websocket();

        /* Each object in listeners is expected to be:
         * {
         *   callback: <callback_fn>,
         * }
         */
        var listeners = [];
       
        var last_response = null;

        function NewConnection() {
            _websocket.Connect(SERVER_API);
            _websocket.SetHandler('message', NotifyListeners);
            _websocket.SetHandler('close', function(a){console.log(a);});
        }

        function NotifyListeners(response) {
            console.log("RECEIVED :>" + response.data);
            last_response = response;
            listeners.map(function (listener) {
                listener.callback(response);
            })
        }

        function AddListener(callback) {
            listeners.push({callback: callback});
            // Send the last response
            console.log('adding listener.....', last_response);
            if (last_response) {
                listeners.map(function (listener) {
                    listener.callback(last_response);
                })
            }
        }

        function Close() {
            _websocket.Close();
        }

        return {
            // The OfflineAdapter does not need to provide SendLine.
            SendLine: undefined,

            // The callback will be invoked on any incoming message.
            // The callback will be passed a new message.
            AddListener: AddListener,

            // Create a new connection to bughouseclub.com.
            // Throws away the old connection.
            NewConnection: NewConnection,

            // Close the connection.
            Close: Close,
        };
    }]);
})();
