/**
 * Service for chatting to people
 */

(function(){
    angular.module('mater2.services').service('LobbyChatService',
      ['IcsService', 'ProfileService', 'ImcService', 'DesktopNotifs', 
      function(IcsService, ProfileService, ImcService, DesktopNotifs) {

        var _lobby = {
            messages: []
        };

        function ToldLobbyMessage(response){
            var regexp = /\(told ([A-Za-z]*)(, who is playing\)|\))/;
            var m = regexp.exec(response.data);
            var name = m[1];
            _lobby.messages.push({
                name: ProfileService.GetUsername(),
                msg: chatGroup.sentText
            })
            chatGroup.sentText = '';
        }

        function ReceivedLobbyMessage(response){
            console.log(response);
            _lobby.messages.push({
                name: response.parsed.username,
                msg: response.parsed.message,
                time: moment().format('h:mm')
            });
            DesktopNotifs.FireNotification({
              title: "Bughouse Club: New message",
              message: response.parsed.message,
            });
        }

        function SendMsg(data){
            var cmd = 'tell 24 ' + data.msg;
            IcsService.SendLine(cmd);
        }

        // Listen for session-related events.
        IcsService.AddListener("told someone message", ToldLobbyMessage);
        IcsService.AddListener("received lobby message", ReceivedLobbyMessage);

        return {
            // Sends a message
            SendMsg : SendMsg,

            //chat groups
            lobby : _lobby
        }
    }]);
})();
