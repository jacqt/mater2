(function(){
    angular.module('mater2.controllers').controller('LobbyChatCtrl', 
        ['$scope', 'LobbyChatService', 'LocalizationService', controller]);
    var RETURN_KEY = 13;

    function controller($scope, LobbyChatService, LocalizationService){
        $scope.locale = LocalizationService.Locale;


        $scope.lobby = LobbyChatService.lobby;

        $scope.key_pressed = function(ev){
            if (ev.keyCode == RETURN_KEY){
                $scope.send_msg();
            }
        }

        $scope.send_msg = function(){
            $scope.lobby.sent_text = $scope.lobby.current_text;
            LobbyChatService.SendMsg({
                msg: $scope.lobby.current_text
            });
            $scope.lobby.current_text = '';
        }
    }
})();

