(function(){
    angular.module('mater2.controllers').controller('ChatController', ['$scope', 'ChatService', 'LocalizationService', controller]);
    var RETURN_KEY = 13;

    function controller($scope, ChatService, LocalizationService){
        $scope.locale = LocalizationService.Locale;


        $scope.chatGroups = ChatService.chatGroups;

        $scope.onKeyPress = function(ev, chatGroup){
            if (ev.keyCode == RETURN_KEY){
                $scope.sendMsg(chatGroup);
            }
        }

        $scope.sendMsg = function(chatGroup){
            chatGroup.sentText = chatGroup.currentText;
            ChatService.SendMsg({
                tellTo: chatGroup.talkingTo,
                msg: chatGroup.currentText
            });
            chatGroup.currentText = '';
        }

        $scope.closeTab = function(tab, $event){
            for (var j = 0; j < $scope.chatGroups.length; j++) {
                if (tab.heading == $scope.chatGroups[j].heading) {
                    $scope.chatGroups.splice(j, 1);
                    break;
                }
            }
            if ($event.stopPropagation) $event.stopPropagation();
            if ($event.preventDefault) $event.preventDefault();
            $event.cancelBubble = true;
            $event.returnValue = false;
        }
    }
})();

