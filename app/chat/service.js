/**
 * Service for chatting to people
 */

(function(){
    angular.module('mater2.services').service('ChatService',
        ['IcsService', 'ProfileService', 'ImcService', function(IcsService, ProfileService, ImcService) {

        var _chatGroups = [];
        function findOrCreateChatGroup(name){
            for (var i = 0; i != _chatGroups.length; ++i){
                var chatGroup = _chatGroups[i];
                if (chatGroup.talkingTo == name){
                    return chatGroup;
                }
            }
            var chatGroup = {
                heading: name,
                talkingTo: name,
                currentText: '',
                active: true,
                messages : []
            }
            _chatGroups.push(chatGroup);
            return chatGroup;
        }

        function ToldSomeoneMessage(response){
            var regexp = /\(told ([A-Za-z]*)(, who is playing\)|\))/;
            var m = regexp.exec(response.data);
            var name = m[1];
            var chatGroup = findOrCreateChatGroup(name);
            chatGroup.messages.push({
                name: ProfileService.GetUsername(),
                msg: chatGroup.sentText
            })
            chatGroup.sentText = '';
        }

        function WasToldMessage(response){
            var regexp = /([A-Za-z]+)(\(.*?\))* tells you: (.*)/;
            var m = regexp.exec(response.data);
            var sender = m[1];
            var msg = m[3];
            var chatGroup = findOrCreateChatGroup(sender);
            chatGroup.messages.push({
                name: sender,
                msg: msg
            });
        }

        function SendMsg(data){
            var cmd = 'tell ' + data.tellTo + ' ' + data.msg;
            IcsService.SendLine(cmd);
        }

        // Listen for session-related events.
        IcsService.AddListener("told someone message", ToldSomeoneMessage);
        IcsService.AddListener("was told message", WasToldMessage);


        //I forgot what this is for...
        ImcService.addHandler('open chat group', function(data){
            findOrCreateChatGroup(data.name);
        });
        return {
            // Sends a message
            SendMsg : SendMsg,

            //chat groups
            chatGroups : _chatGroups
        }
    }]);
})();
