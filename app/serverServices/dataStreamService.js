/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 * Angular service that will open a connection to get push data
 *
 * FIXME: Possible memory leak as eventhandlers are never 
 * deregistered 
 */
(function(){
    var sharedSocket = null;
    var onOpenCallbacks = [];
    var onMsgCallbacks = [];
    var onCloseCallbacks = [];
    var eventNameToCallbackList = {
        'open' : onOpenCallbacks,
        'message' : onMsgCallbacks,
        'close' : onCloseCallbacks
    };

    angular.module('mater2.services').service('dataStreamService', function(socketFactory){
        if (sharedSocket == null){
            sharedSocket = socketFactory({
                url: 'http://bughouseclub.com:3000/stream'
            });
            sharedSocket.setHandler('open', function(ev){
                console.log('ok');
                for (var i = 0; i != onOpenCallbacks.length; ++i){
                    onOpenCallbacks[i](ev);
                }
            });
            sharedSocket.setHandler('message', function(ev){
                for (var i = 0; i != onMsgCallbacks.length; ++i){
                    onMsgCallbacks[i](ev);
                }
            });
            sharedSocket.setHandler('close', function(ev){
                for (var i = 0; i != onCloseCallbacks.length; ++i){
                    onCloseCallbacks[i](ev);
                }
            });
        }
        return {
            addHandler: function(eventName, callback){
                eventNameToCallbackList[eventName].push(callback);
            }
        }
    });
})();
