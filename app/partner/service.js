/**
 * Service for offering matches.
 */

(function(){
    angular.module('mater2.services').service('PartnerService',
        ['IcsService', 'ImcService', 'toaster', function(IcsService, ImcService, toaster) {
            var _error = {
                msg: ''
            };

            var _partner_issued = {
                status: false
            };
            function OfferPartner(name){
                IcsService.SendLine('partner ' + name);
            }

            function stripFicsString(s){
                return /(.*)\n/.exec(s)[1];
            }

            function PartnerError (response){
                console.log('partner err');
                console.log(response);
                ThrowError(stripFicsString(response.data));
            }

            function PartnerIssued(response){
                Reset();
                _partner_issued.status = true;
                toaster.pop('success', 'Successfully issued partnership request');
            }

            function ThrowError(msg) {
                _error.msg = msg;
                toaster.pop('error', msg);
            }

            function Reset(){
                _error.msg = '';
                _partner_issued.status = false;
            }

            IcsService.AddListener("partner error message", PartnerError);
            IcsService.AddListener("partnership issued", PartnerIssued);

            return {
                OfferPartner : OfferPartner,
                partner_issued : _partner_issued,
                error : _error,
                Reset: Reset
            };
    }]);
})();
