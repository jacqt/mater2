/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 * A controller for offering a match.
 */
(function(){
    angular.module('mater2.controllers').controller('PartnerCtrl',
        ['$scope', 'PartnerService', 'LocalizationService', '$modalInstance', controller])
    function controller($scope, PartnerService, LocalizationService, $modalInstance){
        $scope.locale = LocalizationService.Locale;
        // Need to make child elements of input. The issue is documented here:
        // http://stackoverflow.com/questions/12618342/ng-model-does-not-update-controller-value
        PartnerService.Reset()
        $scope.input = {
            username: '',
        };

        $scope.Close = function() {
            $modalInstance.dismiss('cancel');
        }

        $scope.CloseAndNotifySuccess = function(){
            $modalInstance.close({ status: 'success' });
        }

        $scope.partner_issued = PartnerService.partner_issued;

        $scope.error = PartnerService.error;
        
        $scope.OfferPartner = function(){
            PartnerService.OfferPartner($scope.input.username);
        }
    }
})();
