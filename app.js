/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 */

var app = angular.module('mater2', [
    'mater2.controllers',
    'mater2.services',
    'ngRoute',
    'ngMdIcons'
]);

angular.module('mater2.controllers', [
    'luegg.directives',
    'ng.chessboard',
    'mater2.services',
    'GlobalDirectives'
]);

angular.module('mater2.services', [
    'bd.sockjs',
    'ngCookies',
    'ngMaterial',
    'ngAnimate',
    'ngAria',
]);



/**
 * Controls the entire app. Named "MaterImproved".
 */
(function(){
    app.config(['$routeProvider', function ($routeProvider) {
        $routeProvider

        // route for the home page
        .when('/', {
            templateUrl: 'app/pages/home/main.html',
            //controller: 'HomeCtrl'
        })

        // route for the dashboard
        .when('/game', {
            templateUrl: 'app/pages/game/main.html',
            //controller: 'DashboardCtrl'
        })

    }]);
    angular.module('mater2.controllers').controller('MaterImproved',
        ['$scope', 'LoginService', 'IcsService', 'ProfileService', 
         'LocalizationService', 'GameService', '$http', 'toaster',
         'GameService',
          controller]);
    function controller($scope, LoginService, IcsService,
                         ProfileService, LocalizationService,
                         GameService, $http, toaster, GameService) {
        $scope.locale = LocalizationService.Locale;
        $scope.GetUsername = ProfileService.GetUsername;
        $scope.RedirectToSignup = function(){
            window.location = 'http://bughouseclub.com/register/';
        }

        var p = $http.get('app/util/locales/general.json');
        p.success(function(general_names){
            $scope.english = general_names.english;
            $scope.russian = general_names.russian;
        })

        $scope.Resign = function(){
            //FIXME
            IcsService.SendLine('resign');
        }

        $scope.ObservingBoards = function(){
            for (var o in GameService.boards){
                return true;
            }
            return false;
        }

        $scope.ReturnToHome = function(){
          window.location = '#/';
        }

        $scope.OpenLoginDialog = function(){
            var modalInstance = $modal.open({
                templateUrl: 'app/login/main.html',
                controller: 'LoginCtrl',
                size: 'md',
            });
            modalInstance.result.then(
                function success() {
                },
                function cancel(){
                    LoginService.GuestLogin();
                });
        }
        LoginService.LoginDefaultSettings();


        IcsService.AddListener("game started", StartGame);

        function StartGame(){
          window.location = '#/game';
        }
    }
})();
