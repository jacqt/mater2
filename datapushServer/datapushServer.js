/* Module that provides a callback scheme for new information from the coinbase exchange
 * using the websocket api
 */ 


// Uses websockets to maintain a live stream of data
(function(){
    var net = require('net');
    var http = require('http');
    var express = require('express');
    var conn = net.connect(5000, 'bughouseclub.com');
    var sockjs = require('sockjs');

    var PORT = 3000;

    var bughouseState = {
        curBugGames: [],
        curPartners: [],
        allPlayers: [],
        name : 'bughouse',
        unparsedString : ''
    };

    var states = [
        bughouseState
    ];

    function parsePartners(data){
        data = data.toString();
        data = data.replace(/\r/g,'');
        data = data.replace(/\n\n/g,'\n');
        var bugdataRegExp = /Bughouse games in progress:([\s\S\n\r]*)Partnerships not playing bughouse:([\s\S\n\r]*)All players:([\s\S\n\r]*)\(\*\) indicates.*/;
        var parsed = bugdataRegExp.exec(data);
        if (parsed == null){
            return false;
        } 
        if (data == bughouseState.unparsedString){
            return true;
        }
        parsed = parsed.map(function(x){return x.trim()});
        bughouseState.unparsedString = data;

        var t1;
        t1 = parsed[1].split('\n');
        t1 = t1.splice(0, t1.length -1);
        bughouseState.curBugGames = t1;

        var t2 = parsed[2].split('\n');
        t2 = t2.splice(0, t2.length -1);
        bughouseState.curPartners = t2;

        var t3 = parsed[3]
            .split('\n')
            .map(function (t){
                return t.split('  ')
                    .map(function(t){
                        return t.trim()
                    });
            });
        t3 = [].concat.apply([], t3)
            .filter(function(t){
                return t != ''
            }) 
        bughouseState.allPlayers = t3.splice(0, t3.length-1);
        pushUpdate(bughouseState);
        return true;
    }

    var parsers = [
        {
            name : 'bughouseInfo',
            parse : parsePartners
        }
    ];

    var loginListener = function(data){
        var data = data.toString();
        if (data.indexOf('login:') > -1){
            conn.write('guest\n\r\n');
        } 
        else if (data.indexOf('Starting BICS session') > -1){
            updateSelf();
            conn.removeListener('data', loginListener);
            conn.on('data', parseData); //swap event listeners
        } 
    }

    conn.on('data', loginListener);


    function updateSelf(){
        conn.write('bu\n');
        setTimeout(updateSelf, 5000);
    }

    function parseData(data){
        for (var i = 0; i != parsers.length; ++i){
            if (parsers[i].parse(data)){
                break;
            }
        }
    }

    function pushUpdate(update){
        for (var i = 0; i != connections.length; ++i){
            var conn = connections[i];
            conn.write(JSON.stringify(update));
        }
    }

    var connections = [];
    var sockjsApp = sockjs.createServer();
    sockjsApp.on('connection', function(conn){
        connections.push(conn);
        for (var i =0; i != states.length; ++i){
            console.log(states);
            console.log(bughouseState);
            conn.write(JSON.stringify(states[i]));
        }
        conn.on('close', function(){
            connections.splice(connections.indexOf(conn), 1);
        });
    });

    var expressApp = express();
    expressApp.all('/', function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With");
        next();
    });
    var server = http.createServer(expressApp);
    sockjsApp.installHandlers(server, {prefix: '/stream'});
    server.listen(3000, '0.0.0.0');
})();
